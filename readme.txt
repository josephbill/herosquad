Working with databases , Postgresql , Java and Spark Web Framework
step one :
install postgres servers
https://www.enterprisedb.com/downloads/postgres-postgresql-downloads : windows
                                                                       linux
                                                                       mac

https://www.guru99.com/download-install-postgresql.html

https://sqlbackupandftp.com/blog/setting-windows-path-for-postgres-tools  : adding as environmental variable

step two
Upon, successful installation add this dependencies to your project
dependencies

    testImplementation group: 'junit', name: 'junit', version: '4.12'
    implementation "com.sparkjava:spark-core:2.6.0"
    implementation"com.sparkjava:spark-template-handlebars:2.5.5"
    implementation 'org.slf4j:slf4j-simple:1.7.21'
    implementation 'org.sql2o:sql2o:1.5.4'
    implementation group: 'org.postgresql', name: 'postgresql', version: '9.4-1201-jdbc41+'
    implementation group: 'org.sql2o', name: 'sql2o', version: '1.5.4+

Step three
In the terminal proceed and create two postgres database the heroSquad db and a test Db for the same

Run this commands on the terminal (on windows this is done via the SQL shell command line)

to Create DB : CREATE DATABASE nameofDB;

Enter command \l to get a list of all databases
Enter command \d to get a list of all tables

To connect to a Database use \c nameofdatabase

Step four

Write a DAO class test
what a DAO ? a data access object is a pattern that provides an abstract interface to some type of database or
other persistence mechanism. By mapping application calls to the persistence layer, the DAO provides some specific
data operations without exposing details of the database

Data persistence :

Data Persistence is a means for an application to persist and retrieve information from a non-volatile storage system.

Non volatile storage is physical storage media that retain the data without electrical power.
This means that no data is lost when the computer or device is powered off.















