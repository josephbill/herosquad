CREATE DATABASE herosquad;
\c herosquad;
CREATE TABLE hero (id SERIAL PRIMARY KEY, name VARCHAR, squadid INTEGER, weakness VARCHAR, power VARCHAR, age INTEGER);
CREATE TABLE squad (id SERIAL PRIMARY KEY, name VARCHAR, max_size INTEGER, cause VARCHAR);
CREATE DATABASE herosquad_test WITH TEMPLATE herosquad;