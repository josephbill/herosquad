import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroesTest {
   //object reference
    Heroes heroes = new Heroes("SpiderMonkey",25,"Monkey Flips","Banana peels");
    //test for object creation
    @Test
    public void checkIfItCreatesInstance(){
        assertEquals(true, heroes instanceof Heroes);
    }
    //test for inputs
    @Test
    public void heroName_checkifheroNameisentered_String(){
        assertEquals("SpiderMonkey",heroes.getName());
    }
    @Test
    public void heroAge_checkifheroAgeisentered_String(){
        assertEquals(25,heroes.getAge());
    }
    @Test
    public void heroPower_checkifheroPowerentered_String(){
        assertEquals("Monkey Flips",heroes.getSpecial_power());
    }
    @Test
    public void heroWeakness_checkifheroWeaknessentered_String(){
        assertEquals("Banana Flips",heroes.getWeakness());
    }

    @Test
    //test if arraylist add instances when an object is created
    public void all_returnsAllInstancesOfHero_true() {
        Heroes heroes = new Heroes("Joseph",24,"Sweeping","cleaning");
        Heroes secondheroes = new Heroes("Seph",24,"Sweeping","cleaning");

        assertTrue(Heroes.all().contains(heroes));
        assertTrue(Heroes.all().contains(secondheroes));
    }


}