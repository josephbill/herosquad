import org.junit.After;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class SquadTest {
    //object ref
    Squad squad = new Squad("SuicideSquad",1,"fight more evil");
    //test for object creation
    @Test
    public void checkIfItCreatesInstance(){
        assertEquals(true, squad instanceof Squad);
    }
    @Test
    public void checkIfnameisentered(){
        assertEquals("Prime Squad",squad.getName());
    }
    @Test
    public void checkIfcauseisentered(){
        assertEquals("fight the suicide squad",squad.getCause());
    }
    @Test
    public void checkIfSizeisEntered(){
        assertEquals(1,squad.getMax_size());
    }
    //after creation can Squad be cleared
    @After
    public void tearDown() {
        Squad.clear(); //clear out all the posts before each test.
    }

    @Test
    public void AllPostsAreCorrectlyReturned_true() {
        Squad post = new Squad("Prime Squad",6,"amazon war");
        Squad otherPost = new Squad ("War Squad",6,"amazon war");
        assertEquals(3, Squad.all().size());
    }

    @Test
    public void AllPostsContainsAllPosts_true() {
        Squad post = new Squad("Prime Squad",6,"amazon war");
        Squad otherPost = new Squad ("War Squad",6,"amazon war");
        assertTrue(Squad.all().contains(post));
        assertTrue(Squad.all().contains(otherPost));
    }

    @Test
    //test if arraylist add instances when an object is created
    public void all_returnsAllInstancesOfSquad_true() {
        Squad firstSquad = new Squad("HeroSquad",10,"Squad for heroes");
        Squad secondSquad = new Squad("HeroSquads",10,"Squad for hero");

        assertTrue(Squad.all().contains(firstSquad));
        assertTrue(Squad.all().contains(secondSquad));
    }

    @Test
    public void updateChangestoSquad() throws Exception {
        Squad squad = new Squad("Squad two",20,"fighting for squads"); //method to set up new squad
        String formerSquadName = squad.getName(); //method to get all squads
        int formerSquadSize = squad.getMax_size(); //method to get all squads
        String formerSquadCause = squad.getCause(); //method to get all squads
        LocalDateTime formerDate = squad.getCreatedAt(); //when a squad is created
        int formerId = squad.getId(); //fetching id of the squad

        squad.update("Squad two",20,"fighting for squads");

        assertEquals(formerId, squad.getId());
        assertEquals(formerDate, squad.getCreatedAt());
        assertEquals(formerSquadName, squad.getName());
        assertEquals(formerSquadSize, squad.getMax_size());
        assertEquals(formerSquadCause, squad.getCause());

    }

    //test for delete
    @Test
    public void deleteDeletesASpecificSquad() throws Exception {
        Squad squad = new Squad("Joseph",10,"Fight for Joseph's");
        Squad otherSquad = new Squad("Joseph 2",5,"Other squads");
        squad.deletePost(); //method to delete
        assertEquals(1, Squad.all().size()); //one is left
        assertEquals(Squad.all().get(0).getId(), 2); //the one that was deleted has the id of 2. Why do we care?
    }

    //deleting all squads
    @Test
    public void deleteAllPostsDeletesAllSquads() throws Exception {
        Squad squad = new Squad("Joseph",10,"Fight for Joseph's");
        Squad otherSquad = new Squad("Joseph 2",5,"Other squads");
        Squad.clear();
        assertEquals(0, Squad.all().size());
    }


}