//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.BeforeClass;
//import org.sql2o.Sql2o;
//
//import java.sql.Connection;
//
//public class Sql2oCategoryDaoTest {
//    private static Sql2oCategoryDao categoryDao; //these variables are now static.
//    private static Sql2oTaskDao taskDao; //these variables are now static.
//    private static Connection conn; //these variables are now static.
//
//    @BeforeClass //changed to @BeforeClass (run once before running any tests in this file)
//    public static void setUp() throws Exception { //changed to static
//        String connectionString = "jdbc:postgresql://localhost:5432/herosquadtest"; // connect to postgres test database
//        Sql2o sql2o = new Sql2o(connectionString, "postgres", "kikidoyouloveme009"); // changed user and pass to null
//        categoryDao = new Sql2oCategoryDao(sql2o);
//        taskDao = new Sql2oTaskDao(sql2o);
//        conn = sql2o.open(); // open connection once before this test file is run
//    }
//
//    @After // run after every test
//    public void tearDown() throws Exception { //I have changed
//        System.out.println("clearing database");
//        categoryDao.clearAllCategories(); // clear all categories after every test
//        taskDao.clearAllTasks(); // clear all tasks after every test
//    }
//
//    @AfterClass // changed to @AfterClass (run once after all tests in this file completed)
//    public static void shutDown() throws Exception { //changed to static and shutDown
//        conn.close(); // close connection once after this entire test file is finished
//        System.out.println("connection closed");
//    }
//}
