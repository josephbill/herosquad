import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Squad {
    public String name;
    public int max_size;
    public String cause;
    private static ArrayList<Squad> instances = new ArrayList<Squad>();
    private int id;
    private ArrayList<Heroes> heroes;
    private LocalDateTime createdAt;

    public Squad(String name, int max_size, String cause) {
        this.name = name;
        this.max_size = max_size;
        this.cause = cause;
        instances.add(this);
        id = instances.size();
        heroes = new ArrayList<Heroes>();
        this.createdAt = LocalDateTime.now();
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMax_size() {
        return max_size;
    }

    public void setMax_size(int max_size) {
        this.max_size = max_size;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public static ArrayList<Squad> all() {
        return instances;
    }
    public static void clear() {
        instances.clear();
    }

    public int getId()
    {
        return id;
    }

    public static Squad find(int id) {
        return instances.get(id - 1);
    }

    public ArrayList<Heroes> getHeroes()
    {
        return heroes;
    }

    public void addHero(Heroes hero) {
        heroes.add(hero);
    }

    public static Squad findById(int id) {
        return instances.get(id - 1);
    }
    //update method
    public void update(String name, int max_size, String cause){
        this.name = name;
        this.max_size = max_size;
        this.cause = cause;
    }

    //delete method
    public void deletePost(){
        instances.remove(id-1); //same reason
    }
}