import java.util.ArrayList;
import java.util.List;

public class Heroes {
    //name, age , special power , weakness
    public String name;
    public int age;
    public String special_power;
    public String weakness;
    private static ArrayList<Heroes> instances = new ArrayList<Heroes>();
    private int id;


    //constructor
    public Heroes(String name, int age, String special_power, String weakness){
        this.name = name;
        this.age = age;
        this.special_power = special_power;
        this.weakness = weakness;
        instances.add(this);
        id = instances.size();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSpecial_power() {
        return special_power;
    }

    public void setSpecial_power(String special_power) {
        this.special_power = special_power;
    }

    public String getWeakness() {
        return weakness;
    }

    public void setWeakness(String weakness) {
        this.weakness = weakness;
    }

    public static ArrayList<Heroes> all() {
        return instances;
    }
    public static void clear() {
        instances.clear();
    }

    public int getId()
    {
        return id;
    }

    public static Heroes find(int id) {
        try {
            return instances.get(id - 1);
        } catch (IndexOutOfBoundsException exception) {
            return null;
        }
    }

    public static boolean findHeroByName(String name)
    {
        boolean isAvailable = false;
        for (int i =0; i<instances.size(); i++)
        {
            if (name.equalsIgnoreCase(instances.get(i).name) )
            {
                isAvailable = true;
            }
        }

        return isAvailable;
    }
}
