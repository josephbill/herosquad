import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import spark.ModelAndView;
import spark.template.handlebars.HandlebarsTemplateEngine;

import static spark.Spark.*;


public class AppRoutes {
    //main
    public static void main(String[] args) {
        get("/", (req, res) -> {
                    Map<String,Object> model = new HashMap<String,Object>();
                    return  new ModelAndView(model,"homepage.hbs");
                }, new HandlebarsTemplateEngine());
        exception(Exception.class, (ex, rq, rs)->{ex.printStackTrace();});

        //forms
        get("/createhero", (req, res) -> {
            Map<String,Object> model = new HashMap<String,Object>();
            return  new ModelAndView(model,"createhero.hbs");
        }, new HandlebarsTemplateEngine());
        exception(Exception.class, (ex, rq, rs)->{ex.printStackTrace();});

        //create hero from squads
        get("/squads/:id/createhero", (req, res) -> {
            Map<String,Object> model = new HashMap<String,Object>();
            //id number for squad hero being added to.
            Squad squad = Squad.find(Integer.parseInt(req.params(":id")));
            model.put("squads",squad);
            return  new ModelAndView(model,"createhero.hbs");
        }, new HandlebarsTemplateEngine());
        exception(Exception.class, (ex, rq, rs)->{ex.printStackTrace();});

        get("/squadform", (req, res) -> {
            Map<String,Object> model = new HashMap<String,Object>();
            return  new ModelAndView(model,"createsquad.hbs");
        }, new HandlebarsTemplateEngine());
        exception(Exception.class, (ex, rq, rs)->{ex.printStackTrace();});

        get("/squadlist", (req, res) -> {
            Map<String,Object> model = new HashMap<String,Object>();
            //retrieving data from session
            model.put("name",req.session().attribute("squad_name"));
            model.put("size",req.session().attribute("squadsize"));
            model.put("squadcause",req.session().attribute("squadcause"));
            model.put("squadHero",req.session().attribute("heroName"));
            ArrayList<Squad> squads = Squad.all();
            ArrayList<Heroes> heroes = Heroes.all();

            model.put("squads", squads);
            model.put("heroes",heroes);
            return  new ModelAndView(model,"squadlist.hbs");
        }, new HandlebarsTemplateEngine());
        exception(Exception.class, (ex, rq, rs)->{ex.printStackTrace();});

        //methods
        //posts
        post("/squadcreate", (request, response) -> {
            Map<String, Object> model = new HashMap<String, Object>();
            String name = request.queryParams("squadname");
            int size = Integer.parseInt(request.queryParams("squadsize"));
            String squadcause = request.queryParams("squadcause");
            //saving to session
            request.session().attribute("squad_name", name);
            request.session().attribute("size", size);
            request.session().attribute("squad_cause", squadcause);
            //creating the object
            Squad squad = new Squad(name,size,squadcause);
            model.put("squad_name", name);
            model.put("size", size);
            model.put("squad_cause", squadcause);
            //model.put("squads",squad);

//            model.put("squadlist", squad);
            return new ModelAndView(model, "squadcreatedsuccess.hbs");
        }, new HandlebarsTemplateEngine());

        //creating hero
        post("/heroes", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            Squad squad = Squad.find(Integer.parseInt(req.queryParams("squadId")));
            String name = req.queryParams("heroname");
            String age = req.queryParams("heroage");
            String special_power = req.queryParams("heroPower");
            String weakness = req.queryParams("heroweakness");


            if (Heroes.findHeroByName(name.trim()))
            {

                model.put("squad",squad);
            }
            else
            {
                Heroes hero = new Heroes(name,Integer.parseInt(age),special_power,weakness);
                squad.addHero(hero);
                req.session().attribute("heroName", name);
                model.put("squad",squad);
            }
            return new ModelAndView(model, "herocreatedsuccess.hbs");
        }, new HandlebarsTemplateEngine());

        //route for updates //squad
        //get: show a form to update a squad
        get("/squads/:id/update", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            int idOfPostToEdit = Integer.parseInt(req.params("id"));
            Squad editSquad = Squad.findById(idOfPostToEdit);
            model.put("editSquad", editSquad);
            return new ModelAndView(model, "newsquad-form.hbs");
        }, new HandlebarsTemplateEngine());

        //posting new details
        post("/squads/:id/update", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            String newName = req.queryParams("squadname");
            int size = Integer.parseInt(req.queryParams("squadsize"));
            String newCause = req.queryParams("squadcause");
            int idOfPostToEdit = Integer.parseInt(req.params("id"));
            Squad editSquad = Squad.findById(idOfPostToEdit);
            editSquad.update(newName,size,newCause); //don’t forget me //update
            return new ModelAndView(model, "editsquadsuccess.hbs");
        }, new HandlebarsTemplateEngine());

        //route for deleting
        get("/squads/:id/delete", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            int idOfPostToDelete = Integer.parseInt(req.params("id")); //pull id - must match route segment
            Squad deleteSquad = Squad.findById(idOfPostToDelete); //use it to find post
            deleteSquad.deletePost();
            return new ModelAndView(model, "successdeletes.hbs");
        }, new HandlebarsTemplateEngine());

        //route to delete all squads
        get("/squads/delete", (req, res) -> {
            Map<String, Object> model = new HashMap<>();
            Squad.clear();
            return new ModelAndView(model, "successdeletes.hbs");
        }, new HandlebarsTemplateEngine());

    }
}
